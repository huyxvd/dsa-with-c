#include <stdio.h>

#define MAX_SIZE 5

typedef struct
{
    int items[MAX_SIZE];
    int top;
} Stack;

// Hàm khởi tạo Stack
void InitializeStack(Stack* stack)
{
    stack->top = -1;
}

// Hàm kiểm tra Stack có rỗng hay không
int IsEmpty(Stack* stack)
{
    return stack->top == -1;
}

// Hàm kiểm tra Stack đã đầy hay chưa
int IsFull(Stack* stack)
{
    return stack->top == MAX_SIZE - 1;
}

// Hàm thêm một phần tử vào đỉnh Stack
void Push(Stack* stack, int data)
{
    // nếu stack đầy thì in ra "Khong the them. Stack da day.\n"
}

// Hàm xóa phần tử ở đỉnh Stack
void Pop(Stack* stack)
{
    // nếu stack rỗng thì in ra "Khong the xoa. Stack rong.\n"
}

// Hàm lấy giá trị của phần tử ở đỉnh Stack
int Peek(Stack* stack)
{
	// nếu stack rỗng thì in ra "Khong the nhin. Stack rong.\n"
}

// Hàm hiển thị giá trị của Stack từ đỉnh đến đáy
void Display(Stack* stack)
{
	// nếu stack rỗng thì in ra "Stack rong.\n"

	// hiển thị stack lên màn hình console từ đỉnh cho tới đáy
	
    // sau khi in stack ra thì xuống dòng cho đẹp
    printf("\n");
}

int main()
{
    Stack stack;
    InitializeStack(&stack);

    Push(&stack, 10);
    Push(&stack, 20);
    Push(&stack, 30);
    Display(&stack); // Kết quả hiển thị trên màn hình console như sau: 30 20 10

    int topValue = Peek(&stack);
    printf("Phan tu o dinh Stack: %d\n", topValue); // Kết quả hiển thị trên màn hình console như sau: Phan tu o dinh Stack: 30

    Pop(&stack);
    Display(&stack); // Kết quả hiển thị trên màn hình console như sau: 20 10

    Push(&stack, 40);
    Push(&stack, 50);
    Push(&stack, 60);
    Display(&stack); // Kết quả hiển thị trên màn hình console như sau: 60 50 40 20 10
    Push(&stack, 70); // Khong the them. Stack da day.
    Display(&stack); // Kết quả hiển thị trên màn hình console như sau: 60 50 40 20 10

    Pop(&stack);
    Pop(&stack);
    Display(&stack); // Kết quả hiển thị trên màn hình console như sau: 40 20 10

    topValue = Peek(&stack);
    printf("Phan tu o dinh Stack: %d\n", topValue); // Kết quả hiển thị trên màn hình console như sau: Phan tu o dinh Stack: 40

    Pop(&stack);
    Pop(&stack);
    Pop(&stack);
    Display(&stack); // Kết quả hiển thị trên màn hình console như sau: Stack rong.

    Pop(&stack); // Kết quả hiển thị trên màn hình console như sau: Khong the xoa. Stack rong.

    topValue = Peek(&stack); // Kết quả hiển thị trên màn hình console như sau: Khong the nhin. Stack rong.

    return 0;
}
